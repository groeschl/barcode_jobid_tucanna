//-------------------------------------------------------------------------------------------------
// Script content for the "Add filename" fixup
//
//-------------------------------------------------------------------------------------------------
// Copyright © 2015 - Four Pees
// Author: David van Driessche
//-------------------------------------------------------------------------------------------------

// Modifications for barcode:
// Florian Groeschl | calibrate workflow consulting GmbH

//-------------------------------------------------------------------------------------------------
// Print loop
//-------------------------------------------------------------------------------------------------

function cchipPrintLoop() {

	// Loop over all pages
	for( var thePageIndex = 0; thePageIndex < cals_doc_info.pages.length; thePageIndex++ ) {

		// Calculate the number of barcodes to add horizontally and vertically
		var thePageBoxInfo = getPageboxInfo( thePageIndex, 'mediabox' );
		var theMediaboxWidth = thePageBoxInfo[4];
		var theMediaboxHeight = thePageBoxInfo[5];
		var theNumberHorizontal = Math.round( (theMediaboxWidth / 72) / 36 );
		var theNumberVertical = Math.round( (theMediaboxHeight / 72) / 24 );
		var theFileName = cals_doc_info.document.name.slice(0, -4);
		$('#gutterinfo_jobID_text').text( theFileName );
		updateBarcodeData( '#template_barcode', theFileName );

		// Make sure we're working 1 on 1
		adjustDocumentSizeToMediabox( thePageIndex );

		// Remove anything with class barcode
		$( '.barcode').remove();

		// Show the template barcode (for when we begin cloning)
		$( '#template_barcode').show();

		// Add the correct amount of barcodes
		addBarcodes( $( '#template_barcode'), theNumberHorizontal, theNumberVertical, thePageIndex );

		// Hide the template grommet for output
 		$( '#template_barcode').hide();

		// Output the current page
		cchip.printPages(1);
	}
}

// This function adds the necessary amount of grommets
//
function addBarcodes( inTemplateBarcode, inNumberHorizontal, inNumberVertical, inPageIndex ) {

	// Calculate the width and height we have (mediabox with a 7.62cm margin substracted all around)
	var thePageBoxInfo = getPageboxInfo( inPageIndex, 'mediabox' );
	var theMediaboxWidth = thePageBoxInfo[4];
	var theMediaboxHeight = thePageBoxInfo[5];
	var theMargin = (7.62 / 2.54) * 72;

	var theMediaboxWidthMinusMargins = theMediaboxWidth - (theMargin * 2);
	var theMediaboxHeightMinusMargins = theMediaboxHeight - (theMargin * 2);

	// Calculate how much spacing we need between grommets
	var theDeltaX = theMediaboxWidthMinusMargins / (inNumberHorizontal-1);
	var theDeltaY = theMediaboxHeightMinusMargins / (inNumberVertical-1);

	// An index for the grommet we're placing
	var theBarcodeIndex = 1;

	// Add grommets vertically - we need to add "inNumberVertical"
	for (var theIndexY = 0; theIndexY < inNumberVertical; theIndexY++) {

		// Add a grommet vertically along the left border
		addBarcode( inTemplateBarcode, 'barcode_' + theBarcodeIndex, anchorPoints.centerMiddle, inPageIndex,
					'mediabox', anchorPoints.leftTop, 
					theMargin, theMargin + (theIndexY * theDeltaY) );
		theBarcodeIndex += 1;

		// Add a grommet vertically along the right border
		addBarcode( inTemplateBarcode, 'barcode_' + theBarcodeIndex, anchorPoints.centerMiddle, inPageIndex,
					'mediabox', anchorPoints.leftTop, 
					theMediaboxWidth - theMargin, theMargin + (theIndexY * theDeltaY) );
		theBarcodeIndex += 1;
	}

	// Add grommets horizontally - we need to add "inNumberHorizontal - 2". The difference is
	// because we don't want to add grommets to the corners twice so we have to skip those
	for (var theIndexX = 1; theIndexX < (inNumberHorizontal-1) ; theIndexX++) {

		// Add a grommet vertically along the left border
		addBarcode( inTemplateBarcode, 'barcode_' + theBarcodeIndex, anchorPoints.centerMiddle, inPageIndex,
					'mediabox', anchorPoints.leftTop, 
					theMargin + (theIndexX * theDeltaX), theMargin );
		theBarcodeIndex += 1;

		// Add a grommet vertically along the right border
		addBarcode( inTemplateBarcode, 'barcode_' + theBarcodeIndex, anchorPoints.centerMiddle, inPageIndex,
					'mediabox', anchorPoints.leftTop, 
					theMargin + (theIndexX * theDeltaX), theMediaboxHeight - theMargin );
		theBarcodeIndex += 1;
	}
}

// This function creates one specific grommet in one specific location
//
function addBarcode( inTemplateBarcode, inID, inElementAnchor, inPageIndex, inPagebox, inPageboxAnchor, inOffsetX, inOffsetY ) {

	// Clone the template
	var theBarcode = inTemplateBarcode.clone().attr('class', 'barcode').attr('id', inID);
	theBarcode.appendTo( 'body' );

	// Position it
	positionElement( '#' + inID, inElementAnchor, inPageIndex, inPagebox, inPageboxAnchor, inOffsetX, inOffsetY );
}












