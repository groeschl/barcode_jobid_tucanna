//-------------------------------------------------------------------------------------------------
// Script content for the "Add filename" fixup
//
//-------------------------------------------------------------------------------------------------
// Copyright © 2015 - Four Pees
// Author: David van Driessche
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Print loop
//-------------------------------------------------------------------------------------------------

function cchipPrintLoop() {

	// Get the current job ID value
	//var theJobID = getVariableValue( 'var_jobid' );
	var theFileName = cals_doc_info.document.name.slice(0, -4);
	$('#gutterinfo_jobID_text').text( theFileName );
	updateBarcodeData( '#template_barcode', theFileName );

	// Loop over all pages
	for( var thePageIndex = 0; thePageIndex < cals_doc_info.pages.length; thePageIndex++ ) {

		// Make sure we're working 1 on 1
		adjustDocumentSizeToMediabox( thePageIndex );

		// Position our HTML elements
		positionElement( '#gutterinfo_jobID_text', anchorPoints.rightBottom, 
						 thePageIndex, 'cropbox', anchorPoints.rightBottom, 
						 -250, -5 );

		positionElement( '#template_barcode', anchorPoints.rightBottom, 
						 thePageIndex, 'cropbox', anchorPoints.rightBottom, 
						 -5, -5 );


		// Output the current page
		cchip.printPages(1);
	}
}

